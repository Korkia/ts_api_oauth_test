import client as rest_client

tradeshift_path = 'https://api.tradeshift.com/tradeshift/rest/external/'


def get_account_info():
        url = tradeshift_path + 'account/info'
        print rest_client.get(url)


if __name__ == "__main__":
    get_account_info()
