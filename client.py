import json
import oauth2 as oauth


##Find the info you need below from the API Access to own Account from the Tradeshift portal
CONSUMER_KEY = 'OwnAccount'
CONSUMER_SECRET = 'OwnAccount'
TOKEN_KEY = ''
TOKEN_SECRET = ''
TENANT_ID = ''


def get_oauth_client():
    consumer = oauth.Consumer(key=CONSUMER_KEY, secret=CONSUMER_SECRET)
    token = oauth.Token(key=TOKEN_KEY, secret=TOKEN_SECRET)
    return oauth.Client(consumer, token)


def get(url):
    client = get_oauth_client()
    http_headers = {'X-Tradeshift-TenantId': TENANT_ID,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'}

    resp, content = client.request(
        url,
        method="GET",
        headers=http_headers
    )
    return json.loads(content)


def post(url, content):
    client = get_oauth_client()
    http_headers = {'X-Tradeshift-TenantId': TENANT_ID,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'}

    resp, content = client.request(
        url,
        method="POST",
        headers=http_headers,
        body=content
    )
    return resp.status


def put(url, content):
    client = get_oauth_client()
    http_headers = {'X-Tradeshift-TenantId': TENANT_ID,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'}

    resp, content = client.request(
        url,
        method="PUT",
        body=str(content),
        headers=http_headers
    )
    return resp.status


def delete(url):
    client = get_oauth_client()
    http_headers = {'X-Tradeshift-TenantId': TENANT_ID,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'}

    resp, content = client.request(
        url,
        method="DELETE",
        headers=http_headers
    )
    return resp.status